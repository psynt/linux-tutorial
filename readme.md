# Linux commands

1. navigating the system
2. creating a new user

## Browsing the file system:

* The filenames in linux are case sensitive

`ls -lh` lists the contents of current directory, similar to dir in dos  
* you can add `ls -a` to also list hidden files.  
* you can also specify a certain direcory to list: `ls /` lists contents of the / (root) folder.  
* A helpful directory to know about is `~`, which is your home folder, in other words, the one with your personal files  

`mkdir name` makes a directory with the name name  

`rm some_file.txt` removes some_file.txt  
`rm -r Doc/` removes directory Doc and everything in it.  

`cd path` to navigate the file system  

## Creating, viewing and editing files

`touch file.ext` momentarily opens file.ext, which means that it creates it, if the file didn't exist previously and changes its last modified date to now if it did exist.
`cat file.ext` outputs the contents of a file 
`less file.txt` a nicer way to read the contents of a file. Up and down arrows, page up, page down, and press q to quit.

### Editing

Some famous command line editors are `nano` `vi`/`vim` and `emacs`.
`Nano` is by far the easiest to pick up and use. It has all of the commands listed at the bottom (Ctrl-X, Y, enter to save and exit).
`vim` is the most different. press INSESRT to move from "command mode" to "insert mode", press ESC to move back to "command mode", in "command mode", type :q to quit, :wq to quit while saving changes, or :!q to quit discarding changes.


## Creating a new user

These commands can either be run as root (in which case the terminal prompt turns from $ to #), or as a normal user with sudo privilages, by add the command `sudo` in front of each of these following commands:

`adduser bob` to create user bob
`passwd bob` to set user bob's password
(note that simply calling `passwd` without a username will alow you to set the password for the root user, provided you have the privilages to do so).

`adduser bob my_group` adds user bob to the group my_group
Typically, the accounts with sudo rights will be part of a specific group.
In CentOS, this group seems to be `wheel`. So `adduser bob wheel` grants bob sudo rights.

`su bob` to log in as bob from within the terminal.
(Again, `su` by itself logs you in to root, provided you can type the password)
`exit` to end your current session. If logged in as a different user, this will bring you back to your usual self.

### Root

Root is the most powerful user on the system. It has the possibility of doing a significant amount of damage without meaning to, epsecially seeing as it doesn't need to explicitely type sudo in front of system-altering commands.  
As such, linux users try to spend the least amount of time possible as the root user (the one with the # terminal prompt), and preffer to use an "admin" account with sudo rights instead for system administrative tasks wherever possible.  
Of course, this can be taken one step further and you can use an account without sudo privilages as your normal account, and `su` into a sudo-capable account whenever system-level work needs doing.








